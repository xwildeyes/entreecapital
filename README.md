# Pick the King

This is an assignment program that selects Thor's King.

After installing dependencies with `yarn` or `npm`, `npx ts-node index.ts vikings.json` will insert your file, `vikings.json`, into the db. Running `npx ts-node index.ts` (i.e with no filename) will output Thor's King.
