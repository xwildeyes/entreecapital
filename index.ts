import axios from 'axios'
import cheerio from 'cheerio'
import * as fs from 'fs'
import mysql from 'mysql'

// we wrap main() in async function so we can use await
export async function main() {
  var con = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'thorsDatabase',
    insecureAuth: true,
  })

  con.connect()

  const filename = process.argv[2]

  // if a JSON filename was passed on the cli, insert it into the db. Otherwise, output who's the King.
  if (filename) {
    const vikingsData = JSON.parse(
      fs.readFileSync(filename).toString()
    ) as any[]

    // note: we can use pools to do concurrency, but doing sequentially was chosen because brevity

    vikingsData.map(async vikingData => {
      const { name, allSkills, experience, skills, ...viking } = vikingData

      // put the viking in
      con.query('INSERT INTO vikings SET ?', viking)

      experience.forEach(async ({ title, jobTitle, ...experience }) => {
        con.query('INSERT INTO experience SET ?', {
          vkid: viking.vkid,
          jobTitle: title || jobTitle,
          ...experience,
        })
      })
      skills.forEach(async skill => {
        con.query('INSERT INTO skills SET ?', {
          vkid: viking.vkid,
          ...skill,
        })
      })
    })
  } else {
    console.log('This may take a bit...')
    con.query(
      "SELECT firstName, lastName from vikings JOIN experience ON experience.vkid = vikings.vkid WHERE experience.jobTitle IN ('Prince', 'King', 'Queen') AND experience.dateRange LIKE '%Present'",
      async (err, results) => {
        if (err) throw err
        const king = results[0]

        const actor = await enrich(king.firstName)

        console.log(
          `Thor has chosen!\r\n\r\nHis King of Norway shall be: ${
            king.firstName
          } ${
            king.lastName
          } (Played by: ${actor} [Enriched from imdb]).\r\n\r\nThough there were ${results.length -
            1} contestants with equal right to the throne (one battle and is currently royalty).`
        )
      }
    )
  }

  con.end()
}

async function enrich(firstName) {
  const $ = cheerio.load(
    (await axios.get('https://www.imdb.com/title/tt2306299/fullcredits')).data
  )

  const characterElement = $('.cast_list .character a:first-child').filter(
    (index, element) =>
      $(element)
        .text()
        .includes(firstName)
  )

  const actorElement = characterElement
    .parent()
    .parent()
    .find('.primary_photo + td')
    .slice(0, 1)

  return actorElement.text().trim()
}

if (require.main === module) main()
