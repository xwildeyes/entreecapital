DROP DATABASE thorsDatabase;

CREATE DATABASE thorsDatabase;

USE thorsDatabase;

CREATE TABLE vikings (
  vkid VARCHAR(39) NOT NULL,
  firstName VARCHAR(124),
  lastName VARCHAR(124),
  sourceUrl VARCHAR(256),
  numberOfVikingFriends SMALLINT,
  imgUrl VARCHAR(256),
  `timestamp` VARCHAR(24),
  PRIMARY KEY (vkid)
);


CREATE TABLE experience (
  id int NOT NULL AUTO_INCREMENT,
  vkid VARCHAR(39) NOT NULL,
  jobTitle VARCHAR(124),
  `name` VARCHAR(124),
  dateRange VARCHAR(24),
  description VARCHAR(1024),
  url VARCHAR(1024),
  PRIMARY KEY (id),
  FOREIGN KEY (vkid) REFERENCES vikings(vkid)
);

CREATE TABLE skills (
  id int NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(124),
  vkid VARCHAR(39) NOT NULL,
  `rank` int,
  PRIMARY KEY (id),
  FOREIGN KEY (vkid) REFERENCES vikings(vkid)
);
